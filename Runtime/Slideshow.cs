﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DigitalPenguin.Unity
{
    [AddComponentMenu("Image Slideshow")]
    public class Slideshow : MonoBehaviour
    {
        public enum AfterAction
        {
            [InspectorName("Load Scene")]
            LOADSCENE,
            [InspectorName("Animation Trigger")]
            ANIMATIONTRIGGER
        }

        [Header("Slidehow Panels")]
        public GameObject[] panels;

        [Header("Slideshow Options")]
        public bool startImmediately = true;
        public float minDisplayTimePerPanelSeconds = 1;
        public bool continueOnAnyKey = true;
        public KeyCode continueOnKey;
        public bool autoContinue = false;

        [Header("Action on Slideshow End")]
        public AfterAction finishAction;
        public string sceneName;
        public Animator animator;
        public string animationTriggerName;

        private byte activeIndex = 0;

        private float timer = 0;

        private bool started = false;
        private bool locked = false;

        void Awake()
        {
            if (!HasPanels())
            {
                panels = new GameObject[transform.childCount];
                for (int i = 0; i < transform.childCount; i++)
                {
                    panels[i] = transform.GetChild(i).gameObject;
                }
            }
            if (!HasPanels())
            {
                Debug.LogWarning("No Images found for Slideshow Component");
            }
            if (finishAction == AfterAction.ANIMATIONTRIGGER && animator == null)
            {
                animator = GetComponent<Animator>();
                if (animator == null)
                {
                    Debug.LogWarning("No Animator has been set");
                }
            }
            DisableAll();
            ResetTimer();
        }

        private bool HasPanels()
        {
            return panels != null && panels.Length > 0;
        }

        private void DisableAll()
        {
            for (int i = 0; i < panels.Length; i++)
            {
                panels[i].SetActive(false);
            }
        }

        private void ResetTimer()
        {
            if (minDisplayTimePerPanelSeconds < 0)
            {
                minDisplayTimePerPanelSeconds = 0;
            }
            timer = minDisplayTimePerPanelSeconds;
        }

        void Start()
        {
            if (startImmediately)
            {
                StartSlideshow();
            }
        }

        public void StartSlideshow()
        {
            activeIndex = 0;
            panels[activeIndex].SetActive(true);
            started = true;
            locked = true;
        }
        public void NexSlide()
        {
            if (activeIndex >= 0)
            {
                panels[activeIndex].SetActive(false);
            }
            if (activeIndex < (panels.Length - 1))
            {
                activeIndex++;
                panels[activeIndex].SetActive(true);
                locked = true;
                ResetTimer();
            }
            else
            {
                DoAction();
            }
        }

        void Update()
        {
            if (!locked && IsContinueInput())
            {
                NexSlide();
            }

            if (started)
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    locked = false;
                    if (autoContinue)
                    {
                        NexSlide();
                    }
                }
            }
        }

        private bool IsContinueInput()
        {
            bool continueAnyPressed = continueOnAnyKey && Input.anyKeyDown;
            bool continueKeyPressed = Input.GetKeyDown(this.continueOnKey);
            return continueAnyPressed || continueKeyPressed;
        }

        private void DoAction()
        {
            if (finishAction == AfterAction.LOADSCENE && this.sceneName != null)
            {
                SceneManager.LoadScene(sceneName);
            }
            else if (finishAction == AfterAction.ANIMATIONTRIGGER && this.animationTriggerName != null)
            {
                Animator animator = GetComponent<Animator>();
                if (animator == null)
                {
                    Debug.LogWarning("No Animator found.");
                    return;
                }
                animator.SetTrigger(animationTriggerName);
            }
        }
    }
}